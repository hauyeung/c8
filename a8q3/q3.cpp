#include <stdio.h>
#define PI 3.141592654
double area_of_circle( double radius )
{
	static double largest_area = -1.0;
	double area;
	area = PI*radius*radius;
	printf("The area is %lf\n",area);
	if( area > largest_area )
	{
		printf("This is the largest area so far!\n");
		largest_area = area;
	}
	else if( largest_area > -1.0 )
	{
		printf("I've seen bigger circles\n");
	}
	return( area );
}

int main()
{
	area_of_circle(1.0);
	area_of_circle(5.0);
	area_of_circle(2.0);
	return 0;
}