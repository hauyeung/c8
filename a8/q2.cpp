#include <stdio.h>
#include<string.h>

struct name { char firstname[256]; char middlename[256]; char lastname[256]; };
struct person { struct name n; int ssn;} people[6];

void printlist(struct person ppl[], int size)
{
	for (int i=0; i < size; i++)
	{
		if (((char) ppl[i].n.middlename[0]) <= 64 || ((char) ppl[i].n.middlename[0]) >=123)
			printf("%s, %s - %i\n", ppl[i].n.lastname, ppl[i].n.firstname, ppl[i].ssn);
		else
			printf("%s, %s %.1s. - %i\n", ppl[i].n.lastname, ppl[i].n.firstname, ppl[i].n.middlename, ppl[i].ssn);
	}
}


int main(void)
{	
	strcpy(people[0].n.firstname, "John");
	strcpy(people[0].n.middlename, "A");
	strcpy(people[0].n.lastname, "Doe");
	people[0].ssn = 8275770;

	strcpy(people[1].n.firstname, "Jane");
	strcpy(people[1].n.middlename, "B");
	strcpy(people[1].n.lastname, "Smith");
	people[1].ssn = 6986321;

	strcpy(people[2].n.firstname, "Sebastian");
	strcpy(people[2].n.middlename, "Kaeden");
	strcpy(people[2].n.lastname, "Sargent");
	people[2].ssn = 3470380;

	strcpy(people[3].n.firstname, "Adrien");
	strcpy(people[3].n.lastname, "Olguin");
	people[3].ssn = 6975718;

	strcpy(people[4].n.firstname, "Rihanna");
	strcpy(people[4].n.lastname, "Dirkse");
	people[4].ssn = 6951805;

	strcpy(people[5].n.firstname, "Jill");
	strcpy(people[5].n.lastname, "Dickson");
	people[5].ssn = 7902405;


	printlist(people, 6);

	return 0;
}

